<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('beranda', function () {
    return view('Tugas10.beranda');
});
Route::get('login', function () {
    return view('Tugas10.login');
});
Route::get('register', function () {
    return view('Tugas10.register');
});