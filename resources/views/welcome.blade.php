<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Ini adalah link yang digunakan terkait dengan boostrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <!-- Ini adalah link yang digunakan untuk jquary -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
        <!-- Ini adalah link yang digunakan untuk pooper -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" ></script>
        <!-- Ini adalah li yang digunakan untuk javascrip boostrap-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" ></script>

        <title>PELATIHAN WEB DESAIN</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">  


        <!-- Styles -->
        <style>
            html, body {
                background-color: #ffff;
                color: #708090;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 70vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px; font-size: 50px;
            }
            body {background-image: url(gambar.jpg); height: 100vh;
                background-size: cover;
                background-position: center}
        </style>
    </head>
    <body>
       
        </body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    PELATIHAN WEB DESAIN 2020 <br>
                    BY DISNAKER <br>
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">About Us</a>
                    <a href="https://laracasts.com">Contact</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Galery</a>
                </div>
            </div>
        </div>
        <h3 align=center>DATA PESERTA PELATIHAN WEB DESAIN 2020</h3>
        <table border=1 cellpadding=3 cellspacing=3
            align=center>
            <tr align="center">
                <th>Nama Lengkap</th>
                <th>Alamat</th>
                <th>Nomor Telfon</th>
            </tr>
            <tr>
                <th>Amalya Nisa Pratiwi </th>
                <th>Jl Moch Toha Gang Curug Candung</th>
                <th>089658345286</th>
            </tr>
            <tr>
                <th>Ilham Prasetyo</th>
                <th>Sarijadi Blok 6 No. 78 Rt 08 Rw 03</th>
                <th>085858525226</th>
            </tr>
            <tr>
                <th>Shidiq Andhika</th>
                <th>Mekar Jelita No. 10 Rt. 15 Rw. 10</th>
                <th>082214022320</th>
            </tr>
            <tr>
                <th>Rina Nur Baiti</th>
                <th>Jl. Holis No.89/82 RT 08 RW 01</th>
                <th>085862836850</th>
            </tr>
            <tr>
                <th>Lukman Nurmukti Slamet</th>
                <th>Jl. Galunggung Dalam 1 No. 13 </th>
                <th>0853118129520</th>
            </tr>
            <tr>
                <th>Yugho Sembodo</th>
                <th>Blk SMAN 24 No.6 RT 01/01 Ujung Berung </th>
                <th>0817629137</th>
            </tr>
            <tr>
                <th>Finka Anjani Sholihat </th>
                <th>Jl Sukagalih dlm 2 no 134 RT 06 RW 08 </th>
                <th>083129468330</th>
            </tr>
            <tr>
                <th>Muhammad Ridwan suriadikusumah </th>
                <th>Jl. Gading Utara VI no.17-8 </th>
                <th>082115511898</th>
            </tr>
            <tr>
                <th>Hikmat Juyusman Nugraha </th>
                <th>Jl.Cipamokolan Kec.Rancasari Bandung </th>
                <th>089655657153</th>
            </tr>
        </table>
        <footer>
<div class="footer-content">
    </div>
    <div class="content-social">
        <h6> Hubungi Saya </h6>
        <ul>
            <li><a href="#"> Facebook Amalya N</a></li>
            <li><a href="#"> Twitter @amalya </a></li>
            <li><a href="#"> Instagram @amalya</a></li>
        </ul>
    </div>
    <div class="content-about">
        <p>&copy amalya- Web Desain 2020 </p>
</div>
</footer>
    </body>
</html>
