<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Ini adalah link yang digunakan terkait dengan boostrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Ini adalah link yang digunakan untuk jquary -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
    <!-- Ini adalah link yang digunakan untuk pooper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" ></script>
    <!-- Ini adalah li yang digunakan untuk javascrip boostrap-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" ></script>

        <title>PELATIHAN WEB DESAIN</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">  


        <!-- Styles -->
        <style>
            html, body {
                background-color: #000000;
                color: #FFF8DC;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 70vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px; font-size: 40px;
            }
            body {background-image: url(image/name.jpg); height: 100vh;
                background-size: cover;
                background-position: center}
            
            </style>
    </head>
    <body>
       
        </body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    75 TAHUN <br>
                    INDONESIA MAJU. <br>
                    BANGGA BUATAN INDONESIA<br>
                </div>

                <div class="links">
                    <a href="login">LOGIN PESERTA</a>
                    <a href="register">REGISTRASI PERLOMBAAN</a>
            </div>
        </div>
        </div>
        <section class="details-card">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/karung.jpg" width="350px"; height="200px">
                        <span><h4>Lomba Balap Karung</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Lomba balap karung, adalah salah satu perlombaan tradisional yang populer. Lomba kali ini peserta diwajibkan menggunakan helm untuk keamanan kepala.</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/kerupuk.jpg" width="350px"; height="200px">
                        <span><h4>Lomba Makan Kerupuk</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Perlombaan ini akan di ikuti oleh peserta peserta yang telah melakukan registrasi, kemudian akan di lombakan berdasarkan tinggi badan yang sama.</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/tumpeng.jpeg" width="350px"; height="200px">
                        <span><h4>Kreasi Tumpengan Grup</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Tumpeng merupakan bagian dari ritual mengucapkan syukur.Perlombaan ini akan diikuti oleh masing masing grup yang memiliki tim 6 orang</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/jenga.jpg" width="350px"; height="200px">
                        <span><h4>Jenga Raksasa</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Jenga merupakan permainan untuk mengasah kemampuan mental dan fisik. Peserta akan di pasangkan masing masing 2 orang</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/makeup.jpg" width="350px"; height="200px">
                        <span><h4>Blindfolded Makeup</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Perlombaan ini melibatkan 2 orang. Yang satu berperan sebagai MUA dengan menggunakan penutup mata, kemudian pasangannya akan menjadi medianya</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/voli.jpeg" width="350px"; height="200px">
                        <span><h4>Voli Air</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Voli kali ini akan di laksanakan di dalam kolam berenang. Peserta di wajibkan menggunakan baju yang nyaman namun tidak terlalu terbuka.</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/futsal.jpg" width="350px"; height="200px">
                        <span><h4>Futsal Sarung</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Futsal Sarung akan di mainkan oleh tim masing masing dengan berjumlah 2 orang. Dibutuhkan kerjasama yang baik untuk memamainkannya</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/perang.jpg" width="350px"; height="200px">
                        <span><h4>Perang Bantal</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Dua orang pemain duduk di atas sebuah batang pohon yang diletakkan di atas air. Pemain duduk berhadapan dan baku pukul sampai salah satu terjatuh ke air.</p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="image/benteng.jpg" width="350px"; height="200px">
                        <span><h4>Labirin Benteng Takeshi</h4></span>
                    </div>
                    <div class="card-desc">
                        <p>Pengunjung akan berjalan pada lorong selebar 1 meter yang diapit dinding setinggi 2,5 m. Liku-liku lorong sejak pintu masuk hingga pintu keluar. </p>
                            <a href="#" class="btn-card">Read More</a>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    </body>
</html>
