<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Ini adalah link yang digunakan terkait dengan boostrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <!-- Ini adalah link yang digunakan untuk jquary -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
        <!-- Ini adalah link yang digunakan untuk pooper -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" ></script>
        <!-- Ini adalah li yang digunakan untuk javascrip boostrap-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" ></script>

        <title>PELATIHAN WEB DESAIN</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">  


        <!-- Styles -->
        <style>
           body{position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);}
  }

        </style>
    </head>
    <body>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="span3">
    <h3>Registrasi Perlombaan</h3>
    <form>
    <label>Nama Lengkap</label>
    <input type="text" name="firstname" class="span3">
    <label>Alamat</label>
    <input type="text" name="Address" class="span3">
    <label>Nomor Handphone</label>
    <input type="text" name="Nomor" class="span3">
    <label>Email</label>
    <input type="email" name="email" class="span3">
    <label>Kata Sandi</label>
    <input type="password" name="password" class="span3">
    <label>Jenis Kelamin</label>
    <select class="span3">
        <option>Pria</option>
        <option>Wanita</option>
    </select>
    <label>Jenis Perlombaan Yang Diikuti</label>
    <label><input type="checkbox" name="daftar-lomba"> Balap Karung</label>
    <label><input type="checkbox" name="daftar-lomba"> Makan Kerupuk</label>
    <label><input type="checkbox" name="daftar-lomba"> Kreasi Tumpengan (Group)</label>
    <label><input type="checkbox" name="daftar-lomba"> Jenga Raksasa</label>
    <label><input type="checkbox" name="daftar-lomba"> Blindfolded Makeup</label>
    <label><input type="checkbox" name="daftar-lomba"> Voli Air</label>
    <label><input type="checkbox" name="daftar-lomba"> Futsal Sarung</label>
    <label><input type="checkbox" name="daftar-lomba"> Perang Bantal</label>
    <label><input type="checkbox" name="daftar-lomba"> Benteng Takesi</label>
    <input type="submit" value="Submit" class="btn btn-primary pull-right">
    <div class="clearfix"></div>
    </form>
</div>
</div>
    </body>
</html>
